import React from 'react';
import {
    BrowserRouter as Router,
} from "react-router-dom";
import './App.scss';
import Header from './components/Header'
import Main from './views/Main'

function App() {
  return (
    <div className="App">
        <Router>
            <Header/>
        </Router>
        <Main/>
    </div>
  );
}

export default App;
