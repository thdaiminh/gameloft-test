import React, {useEffect, useState} from 'react';
import EventSlider from "../components/EventSlider";

const EventPromotion = ({isMobile = false}) => {
    return(
        <div className={!isMobile ? "event__wrapper full-screen": "event__wrapper"}>
            <div className="event__text main-content">
                <div className="event__text-title">
                    Special Events & Promotional
                </div>
                <div className="event__text-desc">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
                    sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos.
                </div>
            </div>
            <div className="event__carousel">
                <EventSlider isMobile={isMobile}/>
            </div>
        </div>
    );
}

export default EventPromotion;
