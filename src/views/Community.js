import React, {useEffect, useState} from 'react';
import LiveStory from "../components/LiveStory";
import { ReactComponent as Twitter } from '../assets/images/social/twitter-round-blue-bg.svg'
import { ReactComponent as Facebook } from '../assets/images/social/facebook-blue-bg.svg'
import { ReactComponent as Instagram } from '../assets/images/social/instagram-blue-bg.svg'
import { ReactComponent as SearchIcon } from '../assets/images/icon/search.svg'
import AllPosts from "../components/AllPosts";

const Community = ({isMobile = false}) => {
    const [type, setType] = useState(null);
    const [haveFilteredPost, setFilteredPost] = useState(true);
    const [keyword, setKeyword] = useState(null);
    const handleKeyword = (e) => {
        setKeyword(e.currentTarget.value);
    }
    return(
        <div className={!isMobile ? "community__wrapper full-screen": "community__wrapper"}>
            <div className="community__live main-content">
                <div className="community__live-title">
                    Game community hub
                </div>
                {
                    !isMobile &&
                    <div className="community__live-title-sm">
                        Live Game Updates
                    </div>
                }
                <div className="community__live-story">
                    <LiveStory/>
                </div>
            </div>
            <div className="community__post main-content">
                {
                    !isMobile &&
                    <div className="community__post-tool-bar row">
                        <div className="community__post-title col-sm-6">
                            All Posts
                        </div>
                        <div className="community__post-tool col-sm-6 d-flex justify-content-end align-items-center">
                            <div className="community__post-filter d-flex">
                                <div className="community__post-filter-item" onClick={() => setType(3)}>
                                    <Twitter/>
                                </div>
                                <div className="community__post-filter-item" onClick={() => setType(2)}>
                                    <Instagram/>
                                </div>
                                <div className="community__post-filter-item" onClick={() => setType(1)}>
                                    <Facebook/>
                                </div>
                            </div>
                            <div className="community__post-search d-flex">
                                <input type="text" placeholder="Search" onChange={handleKeyword}/>
                                <SearchIcon class="inline-icon"/>
                            </div>
                        </div>
                    </div>
                }
                {
                    isMobile &&
                    <div className="community__post-tool-bar">
                        <div className="community__post-title d-flex align-items-center justify-content-between">
                            All Posts
                            <div className="community__post-tool d-flex justify-content-end">
                                <div className="community__post-filter d-flex">
                                    <div className="community__post-filter-item" onClick={() => setType(3)}>
                                        <Twitter/>
                                    </div>
                                    <div className="community__post-filter-item" onClick={() => setType(2)}>
                                        <Instagram/>
                                    </div>
                                    <div className="community__post-filter-item" onClick={() => setType(1)}>
                                        <Facebook/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="community__post-search d-flex">
                            <input type="text" placeholder="Search" onChange={handleKeyword}/>
                            <SearchIcon class="inline-icon"/>
                        </div>
                    </div>
                }
                <div className={!isMobile ? "community__post-item no-margin": "community__post-item"}>
                    <AllPosts keyword={keyword} type={type} isMobile={isMobile} emtyPost={() => setFilteredPost(false)} havingPost={() => setFilteredPost(true)}/>
                    {
                        isMobile && haveFilteredPost &&
                        <div className="community__post-button" >
                            <a>More</a>
                        </div>
                    }
                </div>
            </div>
        </div>
    );
}

export default Community;
