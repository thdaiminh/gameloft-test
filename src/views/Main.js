import React, {useEffect, useState} from 'react';
import ReactPageScroller from 'react-page-scroller';
import HotspotBanner from './HotspotBanner'
import ContactForm from './ContactForm'
import Community from "./Community";
import EventPromotion from "./EventPromotion";
import ExclusiveContent from "./ExclusiveContent";
import {BrowserRouter as Router} from "react-router-dom";
import Footer from "../components/Footer";

const Main = () => {
    const [isMobile, setMobile] = useState(false);
    useEffect(() => {
        const userAgent =
            typeof window.navigator === "undefined" ? "" : navigator.userAgent;
        const mobile = Boolean(
            userAgent.match(
                /Android|BlackBerry|iPhone|iPod|Opera Mini|IEMobile|WPDesktop/i
            )
        );
        setMobile(mobile);
    }, []);
    if (isMobile) {
        return (
            <div className="main-content__wrapper">
                <HotspotBanner isMobile={true}/>
                <ContactForm isMobile={true}/>
                <Community isMobile={true}/>
                <EventPromotion isMobile={true}/>
                <ExclusiveContent isMobile={true}/>
                <Router>
                    <Footer isMobile={true}/>
                </Router>
            </div>
        );
    }
    else {
        return(
            <div className="main-content__wrapper">
                <ReactPageScroller>
                    <HotspotBanner/>
                    <ContactForm/>
                    <Community/>
                    <EventPromotion/>
                    <ExclusiveContent/>
                    <Router>
                        <Footer/>
                    </Router>
                </ReactPageScroller>
            </div>
        );
    }

}

export default Main;
