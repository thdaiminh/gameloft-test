import React, {useEffect, useState} from 'react';
import ExclusiveSlider from "../components/ExclusiveSlider";

const ExclusiveContent = ({isMobile = false}) => {
    return(
        <div className={!isMobile ? "exclusive-content__wrapper full-screen": "exclusive-content__wrapper"}>
            <div className="exclusive-content__text main-content">
                <div className="exclusive-content__text-title">
                    Exclusive Game Content
                </div>
                <div className="exclusive-content__text-desc">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
                    sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos.
                </div>
            </div>
            <div className="event__carousel">
                <ExclusiveSlider/>
            </div>
        </div>
    );
}

export default ExclusiveContent;
