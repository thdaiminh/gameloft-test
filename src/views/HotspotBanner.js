import React, {useEffect, useState} from 'react';
import TextTruncate from "react-text-truncate";
import { ReactComponent as Thumbnail } from '../assets/images/banner-thumbnail.svg'
import { ReactComponent as Steam } from '../assets/images/social/steam.svg'
import { ReactComponent as Microsoft } from '../assets/images/social/microsoft.svg'
import { ReactComponent as Nintendo } from '../assets/images/social/nintendo.svg'
import Rating from "../components/Rating";

function HotspotBanner ({isMobile = false}) {
    return(
        <div className={!isMobile ? "banner__wrapper full-screen": "banner__wrapper"}>
            <div className="banner__thumbnail d-flex align-items-center justify-content-center">
                <Thumbnail/>
            </div>
            <div className="banner__text main-content">
                <span className="banner__text-title">Gameloft Games</span><br/>
                <span className="banner__text-category">Racing/Action | </span>
                <span className="banner__text-rating">
                    <Rating currentStar={4}/>
                </span> <br/>
                <div className="banner__text-desc">
                    <TextTruncate
                        line={3}
                        element="span"
                        truncateText="…"
                        text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    Ut enim ad minim veniam, quis nostrud itation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in repr">
                    </TextTruncate>
                </div>
            </div>
            <div className="banner__store">
                <div className={!isMobile ? "main-content banner__store-wrapper d-flex justify-content-between align-items-center": "main-content banner__store-wrapper"}>
                    <div className="banner__store-text align-self-center">
                        Download latest version
                    </div>
                    <div className={!isMobile ? "banner__store-logo d-flex" : "banner__store-logo row"}>
                        <div className="banner_store_logo-item">
                            <Nintendo/>
                        </div>
                        <div className="banner_store_logo-item">
                            <Microsoft/>
                        </div>
                        <div className="banner_store_logo-item">
                            <Steam/>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    );
}

export default HotspotBanner;
