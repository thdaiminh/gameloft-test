import React, {useEffect, useState} from 'react';
import Select, { components } from 'react-select'
import { ReactComponent as Thumbnail } from '../assets/images/minion.svg'
import {ReactComponent as Dropdown} from "../assets/images/icon/dropdown.svg";
import {ReactComponent as Global} from "../assets/images/icon/global.svg";

const ContactForm = ({isMobile = false}) => {
    const customDropdownIndicator = props => {
        return (
            <components.DropdownIndicator {...props}>
                <Dropdown />
            </components.DropdownIndicator>
        );
    };

    const customStyles = {
        control: base => ({
            ...base,
            height: 50,
            backgroundColor: 'transparent',
            border: '1px solid #2699fb',
            color: '#2699fb',
        }),
        options: (provided) => ({
            ...provided,
            color: '#2699fb'
        }),
        singleValue: (provided) => ({
            ...provided,
            color: '#2699fb',
        }),
        placeholder: (provided) => ({
            ...provided,
            color: '#2699fb'
        }),
        indicatorSeparator: (provided) => ({
            ...provided,
            display: 'none'
        }),
    };

    const countries = [
        { value: 'fr', label: 'France'},
        { value: 'vi', label: 'Vietnam'}
    ]
    const platform = [
        { value: 'ios', label: 'iOS'},
        { value: 'android', label: 'Android'},
        { value: 'windows', label: 'Windows' }
    ]
    return(
        <div className={!isMobile ? "contact-form__wrapper full-screen": "contact-form__wrapper"}>
            <div className={!isMobile ? "contact-form__items main-content d-flex align-items-center justify-content-between": "contact-form__items main-content d-flex align-items-center justify-content-center"}>
                {
                    !isMobile &&
                    <div className="contact-form__thumbnail">
                        <Thumbnail/>
                    </div>
                }

                <div className="contact-form__form">
                    <div className="contact-form__form-title">
                        Stay in the Know!
                    </div>
                    <div className="contact-form__desc">
                        Don't get left behind! <br/>
                        Subscribe to our newsletters today!
                    </div>
                    <div className="contact-form__input-wrapper">
                        <div className="contact-form__input">
                            <input type="text" placeholder="Name"/>
                        </div>
                        <div className="contact-form__input">
                            <input type="text" placeholder="Email"/>
                        </div>
                        <div className="contact-form__input">
                            <Select
                                placeholder="Country"
                                options={countries}
                                styles={customStyles}
                                components={ {DropdownIndicator: customDropdownIndicator} }
                            />
                        </div>
                        <div className="contact-form__input">
                            <Select
                                placeholder="Platform"
                                options={platform}
                                styles={customStyles}
                                components={ {DropdownIndicator: customDropdownIndicator} }
                            />
                        </div>
                        <div className="contact-form__input mt-4">
                            <label className="custom-checkbox">
                                By signing up, I confirm that I am 13 years old or older.
                                I agree to the Gameloft Terms and Conditions and I have read the Privacy Policy.
                                <input type="checkbox"/>
                                <span className="checkmark"></span>
                            </label>
                        </div>
                        <div className="contact-form__input">
                            <label className="custom-checkbox">
                                I agree to receive promotional offers relating to all Gameloft games and services.
                                <input type="checkbox"/>
                                <span className="checkmark"></span>
                            </label>
                        </div>
                        <div className="contact-form__input d-flex justify-content-center">
                            <button className="submit-button">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ContactForm;
