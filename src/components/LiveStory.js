import React, {useEffect, useState} from 'react';
import Slider from "react-slick";
import { ReactComponent as Prev } from '../assets/images/icon/btn-left.svg'
import { ReactComponent as Next } from '../assets/images/icon/btn-right.svg'
import { ReactComponent as PrevActive } from '../assets/images/icon/btn-left__active.svg'
import { ReactComponent as NextActive } from '../assets/images/icon/btn-right__active.svg'
import Thumbnail1 from '../assets/images/stories/story-thumbnail-1.svg'
import Thumbnail2 from '../assets/images/stories/story-thumbnail-2.svg'
import Thumbnail3 from '../assets/images/stories/story-thumbnail-3.svg'
import Thumbnail4 from '../assets/images/stories/story-thumbnail-4.svg'
import Thumbnail5 from '../assets/images/stories/story-thumbnail-5.svg'
import Thumbnail6 from '../assets/images/stories/story-thumbnail-6.svg'
import Thumbnail7 from '../assets/images/stories/story-thumbnail-7.svg'
import Thumbnail8 from '../assets/images/stories/story-thumbnail-8.svg'
import Thumbnail9 from '../assets/images/stories/story-thumbnail-9.svg'

function CustomNextArrow(props) {
    const { className, onClick } = props;
    if (className.includes('slick-disabled')) {
        return (
            <Next className={className.toString()} onClick={onClick}/>
        );
    } else {
        return (
            <NextActive className={className.toString()} onClick={onClick}/>
        );
    }
}

function CustomPrevArrow(props) {
    const { className, onClick } = props;
    if (className.includes('slick-disabled')) {
        return (
            <Prev className={className.toString()} onClick={onClick}/>
        );
    } else {
        return (
            <PrevActive className={className.toString()} onClick={onClick}/>
        );
    }
}

function LiveStory () {
    const carouselSettings = {
        dots: false,
        speed: 500,
        slidesToShow: 6,
        slidesToScroll: 1,
        adaptiveHeight: true,
        nextArrow: <CustomNextArrow />,
        prevArrow: <CustomPrevArrow />,
        responsive: [
            {
                breakpoint: 1824,
                settings: {
                    slidesToShow: 6,
                }
            },
            {
                breakpoint: 1224,
                settings: {
                    slidesToShow: 6,
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 6,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 5,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 320,
                settings: {
                    slidesToShow: 3,
                }
            },
        ]
    };
    const storyThumbnail = [
        {
            title: 'Short title',
            thumbnail: Thumbnail1,
            link: 'https://www.gameloft.vn',
        },
        {
            title: 'Short title',
            thumbnail: Thumbnail2,
            link: 'https://gameloft.vn',

        },
        {
            title: 'Short title',
            thumbnail: Thumbnail3,
            link: 'https://gameloft.vn',
        },
        {
            title: 'Short title',
            thumbnail: Thumbnail4,
            link: 'https://gameloft.vn',
        },
        {
            title: 'Short title',
            thumbnail: Thumbnail5,
            link: 'https://gameloft.vn',
        },
        {
            title: 'Short title',
            thumbnail: Thumbnail6,
            link: 'https://gameloft.vn',
        },
        {
            title: 'Short title',
            thumbnail: Thumbnail7,
            link: 'https://gameloft.vn',
        },
        {
            title: 'Short title',
            thumbnail: Thumbnail8,
            link: 'https://gameloft.vn',
        },
        {
            title: 'Short title',
            thumbnail: Thumbnail9,
            link: 'https://gameloft.vn',
        },
    ];
    return(
        <div className="live-story__wrapper">
            <Slider {...carouselSettings}>
                {
                    storyThumbnail.map((item,index)=>
                        <a className="live-story__item" href={item.link} target="_blank" key={index}>
                            <div className="live-story__item-thumbnail-wrapper">
                                <img src={item.thumbnail} className="live-story__item-thumbnail"/>
                            </div>
                            <p className="live-story__item-title">{item.title}</p>
                        </a>
                    )
                }
            </Slider>
        </div>
    );
}

export default LiveStory;
