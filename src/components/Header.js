import React, {useEffect, useState, useRef} from 'react';
import { ReactComponent as GLlogo } from '../assets/images/gl-logo.svg'
import { ReactComponent as Hamburger } from '../assets/images/icon/hamburger.svg'
import StickySocial from "./StickySocial";

function Header () {
    return(
        <div className="header-section__wrapper">
            <div className="header-section__items d-flex justify-content-between align-items-center">
                <div className={'header-section__logo'}>
                    <GLlogo/>
                </div>
                <div className={'header-section__hamburger'}>
                    <Hamburger/>
                </div>
            </div>
            <StickySocial/>
        </div>
    );
}

export default Header;
