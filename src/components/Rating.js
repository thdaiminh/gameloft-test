import React, {useEffect, useState} from 'react';
import { ReactComponent as Star } from '../assets/images/icon/star.svg'
import { ReactComponent as UnratedStar } from '../assets/images/icon/star-unrated.svg'

function Rating ({currentStar}) {
    const totalStar = 5;
    let unRated = totalStar - currentStar;
    return(
        <div className="star-rating">
            {[...Array(currentStar)].map((n, i) => {
                return <Star key={i}/>
            })}
            {
                unRated !== 0 && [...Array(unRated)].map((n, i) => {
                    return <UnratedStar key={i}/>
                })
            }
        </div>
    );
}

export default Rating;
