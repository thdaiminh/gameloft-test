import React, {useEffect, useState} from 'react';
import { ReactComponent as Support } from '../assets/images/icon/social-support.svg'
import { ReactComponent as Mail } from '../assets/images/icon/social-mail.svg'
import { ReactComponent as Link } from '../assets/images/icon/social-link.svg'
import { ReactComponent as LinkActive } from '../assets/images/icon/social-link__active.svg'
import { ReactComponent as MailActive } from '../assets/images/icon/social-mail__active.svg'
import { ReactComponent as SupportActive } from '../assets/images/icon/social-support__active.svg'

const StickySocial = () => {
    const [hoverItem, sethoverItem] = useState(0);
    return(
        <div className="sticky-social__wrapper">
            <div className="sticky-social__item d-flex justify-content-center align-items-center" onMouseEnter={()=> sethoverItem(1)} onMouseLeave={()=> sethoverItem(0)}>
                {
                    hoverItem === 1
                        ? <LinkActive/>
                        :  <Link/>
                }

            </div>
            <div className="sticky-social__item d-flex justify-content-center align-items-center" onMouseEnter={()=> sethoverItem(2)} onMouseLeave={()=> sethoverItem(0)}>
                {
                    hoverItem === 2
                        ? <MailActive/>
                        :  <Mail/>
                }
            </div>
            <div className="sticky-social__item d-flex justify-content-center align-items-center" onMouseEnter={()=> sethoverItem(3)} onMouseLeave={()=> sethoverItem(0)}>
                {
                    hoverItem === 3
                        ? <SupportActive/>
                        :  <Support/>
                }
            </div>
        </div>
    );
}

export default StickySocial;
