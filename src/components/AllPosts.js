import React, {useEffect, useState} from 'react';
import TextTruncate from 'react-text-truncate';
import Slider from "react-slick";
import { ReactComponent as Prev } from '../assets/images/icon/arrow-left.svg'
import { ReactComponent as NextActive } from '../assets/images/icon/arrow-right__active.svg'
import { ReactComponent as PrevActive } from '../assets/images/icon/arrow-left__active.svg'
import { ReactComponent as Next } from '../assets/images/icon/arrow-right.svg'
import { ReactComponent as Facebook } from '../assets/images/social/facebook-blue-bg.svg'
import { ReactComponent as Twitter } from '../assets/images/social/twitter-round-blue-bg.svg'
import { ReactComponent as Instagram } from '../assets/images/social/instagram-blue-bg.svg'
import { ReactComponent as Link } from '../assets/images/icon/social-link.svg'
import { ReactComponent as LinkActive } from '../assets/images/icon/social-link__active.svg'
import Thumbnail from '../assets/images/banner-thumbnail-dark.svg'

function AllPosts ({type, keyword = null, isMobile, emtyPost, havingPost}) {
    let slider = null;
    const postType = {
        FACEBOOK : 1,
        INSTAGRAM: 2,
        TWITTER: 3
    }
    const maxPost = 3;
    const [posts, setPosts] = useState([
        {
            id: 1,
            thumbnail: Thumbnail,
            link: 'https://www.gameloft.vn',
            type: postType.FACEBOOK,
            desc: 'search-key-0 FACEBOOK Lorem ipsum dolor sit amet, sed diam voluptua.sed diam voluptua.sed diam voluptua.sed diam voluptua.sed diam voluptua.sed diam voluptua.sed diam voluptua.sed diam voluptua.sed diam voluptua.',
        },
        {
            id: 2,
            thumbnail: Thumbnail,
            link: 'https://www.gameloft.vn',
            type: postType.INSTAGRAM,
            desc: 'search-key-9 INSTAGRAM Lorem ipsum dolor sit amet, sed diam voluptua.',
        },
        {
            id: 3,
            thumbnail: Thumbnail,
            link: 'https://www.gameloft.vn',
            type: postType.TWITTER,
            desc: 'search-key-1 TWITTER Lorem ipsum dolor sit arat, sed diam voluptua.',
        },
        {
            id: 4,
            thumbnail: Thumbnail,
            link: 'https://www.gameloft.vn',
            type: postType.FACEBOOK,
            desc: 'search-key-2 FACEBOOK Lorem ipsum dolor sit arat, sed diam voluptua.',
        },
        {
            id: 5,
            thumbnail: Thumbnail,
            link: 'https://www.gameloft.vn',
            type: postType.INSTAGRAM,
            desc: 'search-key-3 INSTAGRAM Lorem ipsum dolor sit arat, sed diam voluptua.',
        },
        {
            id: 6,
            thumbnail: Thumbnail,
            link: 'https://www.gameloft.vn',
            type: postType.FACEBOOK,
            desc: 'search-key-4 FACEBOOK Lorem ipsum dolor sit arat, sed diam voluptua.',
        },
        {
            id: 7,
            thumbnail: Thumbnail,
            link: 'https://www.gameloft.vn',
            type: postType.FACEBOOK,
            desc: 'search-key-5  FACEBOOK Lorem ipsum dolor sit arat, sed diam voluptua.',
        },
        {
            id: 8,
            thumbnail: Thumbnail,
            link: 'https://www.gameloft.vn',
            type: postType.INSTAGRAM,
            desc: 'search-key-6 INSTAGRAM Lorem ipsum dolor sit arat, sed diam voluptua.',
        },
        {
            id: 9,
            thumbnail: Thumbnail,
            link: 'https://www.gameloft.vn',
            type: postType.TWITTER,
            desc: 'search-key-7 TWITTER Lorem ipsum dolor sit arat, sed diam voluptua.',
        },
        {
            id: 10,
            thumbnail: Thumbnail,
            link: 'https://www.gameloft.vn',
            type: postType.TWITTER,
            desc: 'search-key-8 TWITTER Lorem ipsum dolor sit arat, sed diam voluptua.',
        },
        {
            id: 11,
            thumbnail: Thumbnail,
            link: 'https://www.gameloft.vn',
            type: postType.TWITTER,
            desc: 'search-key-8 TWITTER Lorem ipsum dolor sit arat, sed diam voluptua.',
        },
        {
            id: 12,
            thumbnail: Thumbnail,
            link: 'https://www.gameloft.vn',
            type: postType.INSTAGRAM,
            desc: 'search-key-8 INSTAGRAM Lorem ipsum dolor sit arat, sed diam voluptua.',
        },
    ]);
    const [filteredPosts, setFilterPost] = useState([]);
    const [hoverItem, sethoverItem] = useState(0);
    const [currentSlide, setSlide] = useState(0);
    const handlePagination = index => {
        setSlide(index);
    }
    useEffect(() => {
        handlePosts();
    }, [type, keyword]);
    const carouselSettings = {
        dots: false,
        arrows: false,
        infinite: true,
        centerMode: true,
        slidesToShow: 5,
        draggable: false,
        responsive: [
            {
                breakpoint: 1824,
                settings: {
                    slidesToShow: 5,
                }
            },
            {
                breakpoint: 1224,
                settings: {
                    slidesToShow: 4,
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                }
            },
            {
                breakpoint: 320,
                settings: {
                    slidesToShow: 1,
                }
            }
        ],
        beforeChange: (current, next) => {
            setTimeout(setSlide(next), 500)
        },
    };
    const carouselFilteredSettings = {
        dots: false,
        arrows: false,
        infinite: true,
        centerMode: true,
        slidesToShow: 3,
        draggable: false,
        beforeChange: (current, next) => {
            setTimeout(setSlide(next), 500)
        },
    };
    const next = () => {
        slider.slickNext();
    }
    const previous = () => {
        slider.slickPrev();
    }

    const handlePosts = () => {
        let reduced = posts.reduce(function(result, post) {
            if (type !== null && keyword !== null) {
                if (post.type === type && post.desc.toUpperCase().includes(keyword.toUpperCase())) {
                    result.push(post);
                }
            } else if (type !== null && keyword === null) {
                if (post.type === type) {
                    result.push(post);
                }
            } else if (keyword !== null && type === null) {
                if (post.desc.toUpperCase().includes(keyword.toUpperCase())) {
                    result.push(post);
                }
            }

            return result;
        }, []);
        reduced.length === 0 && (type !== null || keyword !== null)? emtyPost() : havingPost();
        setFilterPost(reduced);
    }
    if (!isMobile) {
        if (filteredPosts.length === 0 && (type !== null || keyword !== null)) {
            return (
                <div className="all-posts__wrapper no-margin">
                    <div className="all-posts__notification">
                        Your keyword don't match any posts
                    </div>
                </div>
            );
        } else {
            if (type === null && keyword === null) {
                return (
                    <div className="all-posts__wrapper no-margin">
                        <Slider {...carouselSettings} ref={c => (slider = c)}>
                            {
                                posts.map((item,index)=>
                                    <div className="all-posts__item" key={index}>
                                        <div className="all-posts__item-tool-wrapper d-flex justify-content-between">
                                            <div className="all-posts__item-tool-social">
                                                {
                                                    item.type === postType.FACEBOOK ? <Facebook/> :
                                                        item.type === postType.INSTAGRAM ? <Instagram/> :
                                                            item.type === postType.TWITTER ? <Twitter/> : null
                                                }
                                            </div>
                                            <div className="all-posts__item-tool-link d-flex justify-content-center align-items-center" onMouseEnter={()=> sethoverItem(item.id)} onMouseLeave={()=> sethoverItem(0)}>
                                                {
                                                    hoverItem === item.id
                                                        ? <LinkActive/>
                                                        :  <Link/>
                                                }
                                            </div>
                                        </div>
                                        <div className="all-posts__item-thumbnail-wrapper">
                                            <img src={item.thumbnail} className="all-posts__item-thumbnail"/>
                                        </div>
                                        <p className="all-posts__item-desc">
                                            <TextTruncate
                                                line={2}
                                                element="span"
                                                truncateText="…"
                                                text={item.desc}
                                            />
                                        </p>
                                    </div>
                                )
                            }
                        </Slider>
                        <div className="custom-pagination d-flex justify-content-center align-items-center">
                            <div className="custom-pagination__prev">
                                {
                                    currentSlide  === 0 ? <Prev/> : <PrevActive onClick={previous}/>
                                }
                            </div>
                            <div className="custom-pagination__page">
                                {
                                    currentSlide + 1 < 10 ? <span><b>0{currentSlide + 1 }</b> / {posts.length}</span>
                                        : <span><b>{currentSlide + 1 }</b> / {posts.length}</span>
                                }
                            </div>
                            <div className="custom-pagination__next">
                                {
                                    currentSlide + 1 === posts.length ? <Next/> : <NextActive onClick={next}/>
                                }
                            </div>
                        </div>
                    </div>
                )
            } else {
                return (
                    <div className="all-posts__wrapper no-margin">
                        <Slider {...carouselFilteredSettings} ref={c => (slider = c)}>
                            {
                                filteredPosts.map((item,index)=>
                                    <div className="all-posts__item" key={index}>
                                        <div className="all-posts__item-tool-wrapper d-flex justify-content-between">
                                            <div className="all-posts__item-tool-social">
                                                {
                                                    item.type === postType.FACEBOOK ? <Facebook/> :
                                                        item.type === postType.INSTAGRAM ? <Instagram/> :
                                                            item.type === postType.TWITTER ? <Twitter/> : null
                                                }
                                            </div>
                                            <div className="all-posts__item-tool-link d-flex justify-content-center align-items-center" onMouseEnter={()=> sethoverItem(item.id)} onMouseLeave={()=> sethoverItem(0)}>
                                                {
                                                    hoverItem === item.id
                                                        ? <LinkActive/>
                                                        :  <Link/>
                                                }
                                            </div>
                                        </div>
                                        <div className="all-posts__item-thumbnail-wrapper">
                                            <img src={item.thumbnail} className="all-posts__item-thumbnail"/>
                                        </div>
                                        <p className="all-posts__item-desc">
                                            <TextTruncate
                                                line={2}
                                                element="span"
                                                truncateText="…"
                                                text={item.desc}
                                            />
                                        </p>
                                    </div>
                                )
                            }
                        </Slider>
                        <div className="custom-pagination d-flex justify-content-center align-items-center">
                            <div className="custom-pagination__prev">
                                {
                                    currentSlide  === 0 ? <Prev/> : <PrevActive onClick={previous}/>
                                }
                            </div>
                            <div className="custom-pagination__page">
                                {
                                    currentSlide + 1 < 10 ? <span><b>0{currentSlide + 1 }</b> / {filteredPosts.length}</span>
                                        : <span><b>{currentSlide + 1 }</b> / {filteredPosts.length}</span>
                                }
                            </div>
                            <div className="custom-pagination__next">
                                {
                                    currentSlide + 1 === posts.length ? <Next/> : <NextActive onClick={next}/>
                                }
                            </div>
                        </div>
                    </div>
                )
            }
        }
    } else {
        if (filteredPosts.length === 0 && (type !== null || keyword !== null)) {
            return (
                <div className="all-posts__wrapper">
                    <div className="all-posts__notification">
                        Your keyword don't match any posts
                    </div>
                </div>
            );
        } else {
            if (type === null && keyword === null) {
                return (
                    <div className="all-posts__wrapper">
                        {
                            posts.slice(0, maxPost).map((item,index)=>
                                <div className="all-posts__item" key={index}>
                                    <div className="all-posts__item-tool-wrapper d-flex justify-content-between">
                                        <div className="all-posts__item-tool-social">
                                            {
                                                item.type === postType.FACEBOOK ? <Facebook/> :
                                                item.type === postType.INSTAGRAM ? <Instagram/> :
                                                item.type === postType.TWITTER ? <Twitter/> : null
                                            }
                                        </div>
                                        <div className="all-posts__item-tool-link d-flex justify-content-center align-items-center" onMouseEnter={()=> sethoverItem(item.id)} onMouseLeave={()=> sethoverItem(0)}>
                                            {
                                                hoverItem === item.id
                                                    ? <LinkActive/>
                                                    :  <Link/>
                                            }
                                        </div>
                                    </div>
                                    <div className="all-posts__item-thumbnail-wrapper">
                                        <img src={item.thumbnail} className="all-posts__item-thumbnail"/>
                                    </div>
                                    <p className="all-posts__item-desc">
                                        <TextTruncate
                                            line={5}
                                            element="span"
                                            truncateText="…"
                                            text={item.desc}
                                        />
                                    </p>
                                </div>
                            )
                        }
                    </div>
                )
            } else {
                return (
                    <div className="all-posts__wrapper">
                        {
                            filteredPosts.slice(0, maxPost).map((item, index)=>
                                <div className="all-posts__item" key={index}>
                                    <div className="all-posts__item-tool-wrapper d-flex justify-content-between">
                                        <div className="all-posts__item-tool-social">
                                            {
                                                item.type === postType.FACEBOOK ? <Facebook/> :
                                                    item.type === postType.INSTAGRAM ? <Instagram/> :
                                                        item.type === postType.TWITTER ? <Twitter/> : null
                                            }
                                        </div>
                                        <div className="all-posts__item-tool-link d-flex justify-content-center align-items-center" onMouseEnter={()=> sethoverItem(item.id)} onMouseLeave={()=> sethoverItem(0)}>
                                            {
                                                hoverItem === item.id
                                                    ? <LinkActive/>
                                                    :  <Link/>
                                            }
                                        </div>
                                    </div>
                                    <div className="all-posts__item-thumbnail-wrapper">
                                        <img src={item.thumbnail} className="all-posts__item-thumbnail"/>
                                    </div>
                                    <p className="all-posts__item-desc">
                                        <TextTruncate
                                            line={2}
                                            element="span"
                                            truncateText="…"
                                            text={item.desc}
                                        />
                                    </p>
                                </div>
                            )
                        }
                    </div>
                )
            }
        }
    }


}

export default AllPosts;
