import React, {useEffect, useState} from 'react';
import Select, { components } from 'react-select'
import { ReactComponent as GLlogoWhite } from '../assets/images/gl-logo--white.svg'
import { ReactComponent as Facebook } from '../assets/images/social/facebook-white-bg.svg'
import { ReactComponent as Twitter } from '../assets/images/social/twitter-white-bg.svg'
import { ReactComponent as Linkedin } from '../assets/images/social/linkedin-white-bg.svg'
import { ReactComponent as Youtube } from '../assets/images/social/youtube-white-bg.svg'
import { ReactComponent as FacebookMobile } from '../assets/images/social/facebook-trans-blue-bg.svg'
import { ReactComponent as TwitterMobile } from '../assets/images/social/twitter-blue-bg.svg'
import { ReactComponent as LinkedinMobile } from '../assets/images/social/linkedin-blue-bg.svg'
import { ReactComponent as YoutubeMobile } from '../assets/images/social/youtube-blue-bg.svg'
import { ReactComponent as Global } from '../assets/images/icon/global.svg'
import { ReactComponent as Dropdown } from '../assets/images/icon/dropdown.svg'

function Footer ({isMobile = false}) {
    const customDropdownIndicator = props => {
        return (
            <components.DropdownIndicator {...props}>
                <Dropdown />
            </components.DropdownIndicator>
        );
    };

    const customMenuList = props => {
        return (
            <components.MenuList {...props}>
                <div className="input-menu">
                    {props.children}
                </div>
            </components.MenuList>
        );
    };

    const customSingleValue = ({ data }) => (
        <div className="input-select">
            <div className="input-select__single-value">
                { data.icon && <span className="input-select__icon">{ data.icon }</span> }
                <span className="input-select__label">{ data.label }</span>
            </div>
        </div>
    );

    const languages = [
        { value: 'eng', label: 'English', icon: <Global/> },
        { value: 'vi', label: 'Vietnamese', icon: <Global/> }
    ]
    if (!isMobile) {
        return(
            <div className="footer-section__wrapper">
                <div className="footer-section__items">
                    <div className="footer-section__main row">
                        <div className="col-sm-6">
                            <div className={'footer-section__logo'}>
                                <GLlogoWhite/>
                            </div>
                            <div className={'footer-section__spacing-left'}>
                                <p className={'footer-section__follow-text'}>Follow Us</p>
                                <div className={'footer-section__social d-flex'}>
                                    <a className={'footer-section__social-item'} href="https://www.facebook.com/gameloft/" target="_blank">
                                        <Facebook/>
                                    </a>
                                    <a className={'footer-section__social-item'} href="https://twitter.com/gameloft" target="_blank">
                                        <Twitter/>
                                    </a>
                                    <a className={'footer-section__social-item'} href="https://www.linkedin.com/company/gameloft" target="_blank">
                                        <Linkedin/>
                                    </a>
                                    <a className={'footer-section__social-item'} href="https://www.youtube.com/user/gameloft" target="_blank">
                                        <Youtube/>
                                    </a>
                                </div>
                                <div className={'footer-section__languages'}>
                                    <Select
                                        defaultValue={languages[0]}
                                        options={languages}
                                        components={ {SingleValue: customSingleValue, DropdownIndicator: customDropdownIndicator, MenuList: customMenuList  } }
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-6 d-flex justify-content-end">
                            <div className={'footer-section__nav'}>
                                <div className={'footer-section__nav-title'}>Visit</div>
                                <span>Gameloft Games</span><br/>
                                <span>Gameloft Careers</span><br/>
                                <span>Gameloft News</span><br/>
                                <span>Gameloft Forum</span><br/>
                                <span>Gameloft Corporate</span><br/>
                                <span>Gameloft Advertising</span><br/>
                                <span>Gameloft Support</span><br/>
                                <div className={'footer-section__nav-title mt-4'}>Legal</div>
                                <span>Terms of Use</span><br/>
                                <span>Privacy Policy</span><br/>
                                <span>Cookies Policy</span><br/>
                                <span>EULA</span><br/>
                                <span>Legal Notices</span><br/>
                                <span>Event Rules</span><br/>
                                <span>Business Contact</span><br/>
                            </div>
                        </div>
                    </div>
                    <div className="footer-section__border"></div>
                    <div className="footer-section__policy">
                        ©2020 Gameloft. All rights reserved. Gameloft and the Gameloft logo are trademarks of Gameloft in the U.S. and/or other countries.
                        <br/>
                        All other trademarks are the property of their respective owners.
                    </div>
                </div>

            </div>
        );
    } else {
        return(
            <div className="footer-section__wrapper">
                <div className={'footer-section__social-wrapper'}>
                    <p className={'footer-section__follow-text'}>Follow Us</p>
                    <div className={'footer-section__social d-flex justify-content-center'}>
                        <a className={'footer-section__social-item'} href="https://www.facebook.com/gameloft/" target="_blank">
                            <FacebookMobile/>
                        </a>
                        <a className={'footer-section__social-item'} href="https://twitter.com/gameloft" target="_blank">
                            <TwitterMobile/>
                        </a>
                        <a className={'footer-section__social-item'} href="https://www.linkedin.com/company/gameloft" target="_blank">
                            <LinkedinMobile/>
                        </a>
                        <a className={'footer-section__social-item'} href="https://www.youtube.com/user/gameloft" target="_blank">
                            <YoutubeMobile/>
                        </a>
                    </div>
                </div>
                <div className="footer-section__route-wrapper main-content">
                    <div className="footer-section__route-logo d-flex justify-content-center">
                        <GLlogoWhite/>
                    </div>
                    <div className="footer-section__route-nav d-flex justify-content-between">
                        <div className="footer-section__route-nav-item">
                            <span>Gameloft Games</span><br/>
                            <span>Gameloft Careers</span><br/>
                            <span>Gameloft News</span><br/>
                            <span>Gameloft Forum</span><br/>
                            <span>Gameloft Corporate</span><br/>
                            <span>Gameloft Advertising</span><br/>
                            <span>Gameloft Support</span><br/>
                        </div>
                        <div className="footer-section__route-nav-item">
                            <span>Terms of Use</span><br/>
                            <span>Privacy Policy</span><br/>
                            <span>Cookies Policy</span><br/>
                            <span>EULA</span><br/>
                            <span>Legal Notices</span><br/>
                            <span>Event Rules</span><br/>
                            <span>Business Contact</span><br/>
                        </div>
                    </div>
                    <div className="footer-section__policy">
                        ©2020 Gameloft. All rights reserved. Gameloft and the Gameloft logo are trademarks of Gameloft in the U.S. and/or other countries.
                        <br/>
                        All other trademarks are the property of their respective owners.
                    </div>
                </div>
            </div>
        )
    }

}

export default Footer;
