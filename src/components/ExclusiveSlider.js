import React, {useEffect, useState} from 'react';
import Slider from "react-slick";
import { ReactComponent as Prev } from '../assets/images/icon/arrow-left.svg'
import { ReactComponent as NextActive } from '../assets/images/icon/arrow-right__active.svg'
import { ReactComponent as PrevActive } from '../assets/images/icon/arrow-left__active.svg'
import { ReactComponent as Next } from '../assets/images/icon/arrow-right.svg'
import { ReactComponent as Download } from '../assets/images/icon/download.svg'
import Thumbnail2 from '../assets/images/content/content-1.jpg'
import Thumbnail3 from '../assets/images/content/content-2.jpg'
import Thumbnail4 from '../assets/images/content/content-3.png'

function ExclusiveSlider () {
    let slider = null;
    const [posts, setPosts] = useState([
        {
            id: 1,
            thumbnail: Thumbnail2,
        },
        {
            id: 2,
            thumbnail: Thumbnail3,
        },
        {
            id: 3,
            thumbnail: Thumbnail2,
        },
        {
            id: 4,
            thumbnail: Thumbnail4,
        },
        {
            id: 5,
            thumbnail: Thumbnail4,
        },
        {
            id: 6,
            thumbnail: Thumbnail2,
        },
        {
            id: 7,
            thumbnail: Thumbnail4,
        },
        {
            id: 8,
            thumbnail: Thumbnail2,
        },
        {
            id: 9,
            thumbnail: Thumbnail3,
        },
        {
            id: 10,
            thumbnail: Thumbnail4,
        },

    ]);
    const [currentSlide, setSlide] = useState(0);
    const handlePagination = index => {
        setSlide(index);
    }
    const carouselSettings = {
        dots: false,
        arrows: false,
        infinite: true,
        slidesToShow: 3,
        centerMode: true,
        draggable: false,
        responsive: [
            {
                breakpoint: 1824,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 1224,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                }
            },
            {
                breakpoint: 320,
                settings: {
                    slidesToShow: 1,
                }
            },
        ],
        beforeChange: (current, next) => {
            setTimeout(setSlide(next), 500)
        },
    };
    const next = () => {
        slider.slickNext();
    }
    const previous = () => {
        slider.slickPrev();
    }

    return (
        <div className="exclusive-slider__wrapper">
            <Slider {...carouselSettings} ref={c => (slider = c)}>
                {
                    posts.map((item,index)=>
                        <div className="exclusive-slider__item" key={index}>
                            <div className="exclusive-slider__item-thumbnail-wrapper" style={{ backgroundImage: `url(${item.thumbnail})`}}>
                            </div>
                            <div className="exclusive-slider__item-download">
                                <a href={item.thumbnail} download>
                                    <Download/>
                                </a>
                            </div>
                        </div>
                    )
                }
            </Slider>
            <div className="custom-pagination d-flex justify-content-center align-items-center">
                <div className="custom-pagination__prev">
                    {
                        currentSlide  === 0 ? <Prev/> : <PrevActive onClick={previous}/>
                    }
                </div>
                <div className="custom-pagination__page">
                    {
                        currentSlide + 1 < 10 ? <span><b>0{currentSlide + 1 }</b> / {posts.length}</span>
                            : <span><b>{currentSlide + 1 }</b> / {posts.length}</span>
                    }
                </div>
                <div className="custom-pagination__next">
                    {
                        currentSlide + 1 === posts.length ? <Next/> : <NextActive onClick={next}/>
                    }
                </div>
            </div>
        </div>
    )

}

export default ExclusiveSlider;
