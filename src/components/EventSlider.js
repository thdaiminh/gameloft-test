import React, {useEffect, useState} from 'react';
import Slider from "react-slick";
import { CircularProgressbarWithChildren, buildStyles } from 'react-circular-progressbar';
import { ReactComponent as Link } from '../assets/images/icon/social-link.svg'
import { ReactComponent as LinkActive } from '../assets/images/icon/social-link__active.svg'
import Thumbnail from '../assets/images/banner-thumbnail.svg'
import TextTruncate from "react-text-truncate";

function EventSlider ({isMobile}) {
    const [currentSlide, setSlide] = useState(0);
    const [hoverItem, sethoverItem] = useState(null);
    const [hoverItemIcon, sethoverItemIcon] = useState(null);
    const carouselSettings = {
        arrows: false,
        dots: true,
        centerMode: true,
        speed: 500,
        slidesToShow: 5,
        infinite: true,
        autoplay: true,
        centerPadding: 0,
        slidesToScroll: 1,
        beforeChange: (current, next) => {
            setTimeout(setSlide(next), 500)
        },

        customPaging: i => {
            const pagingLength = eventItem.length;
            const pagingProgress =  (i + 1) / pagingLength * 100;
            return (
                <div
                    className="custom-pagination__wrapper"
                >
                    <CustomPagination progress={pagingProgress} current={i}/>
                </div>
            )
        },
        responsive: [
            {
                breakpoint: 1824,
                settings: {
                    slidesToShow: 5,
                    speed: 500,
                }
            },
            {
                breakpoint: 1224,
                settings: {
                    slidesToShow: 4,
                    speed: 500,

                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    speed: 500,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                }
            },
            {
                breakpoint: 320,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    };
    const CustomPagination = (pageInfo) => {
        if(!isMobile) {
            if (pageInfo.current < 9) {
                if (pageInfo.current === currentSlide) {
                    return (
                        <CircularProgressbarWithChildren
                            value={pageInfo.progress}
                            styles={buildStyles({
                                pathColor: `#2699fb`,
                                trailColor: 'white',
                            })}
                        >
                            <div className="custom-pagination__number">
                                0{ pageInfo.current + 1}
                            </div>
                        </CircularProgressbarWithChildren >
                    )
                } else {
                    return (
                        <span className="custom-pagination__number">0{ pageInfo.current + 1}</span>
                    )
                }
            } else {
                if (pageInfo.current === currentSlide) {
                    return (
                        <CircularProgressbarWithChildren
                            value={pageInfo.progress}
                            styles={buildStyles({
                                pathColor: `#2699fb`,
                                trailColor: 'white',
                            })}
                        >
                            <div className="custom-pagination__number">
                                { pageInfo.current + 1}
                            </div>
                        </CircularProgressbarWithChildren >
                    )
                } else {
                    return (
                        <span className="custom-pagination__number">{ pageInfo.current + 1}</span>
                    )
                }
            }
        } else {
            return null
        }
    };
    const eventItem = [
        {
            title: 'Short title here 1',
            desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua'
              + 'Ut enim ad minim veniam, quis nostrud itation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in repr',
            thumbnail: Thumbnail,
            link: 'https://www.gameloft.vn',
        },
        {
            title: 'Short title here 2',
            desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua'
              + 'Ut enim ad minim veniam, quis nostrud itation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in repr',
            thumbnail: Thumbnail,
            link: 'https://www.gameloft.vn',
        },
        {
            title: 'Short title here 3',
            desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua'
              + 'Ut enim ad minim veniam, quis nostrud itation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in repr',
            thumbnail: Thumbnail,
            link: 'https://www.gameloft.vn',
        },
        {
            title: 'Short title here 4',
            desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua'
              + 'Ut enim ad minim veniam, quis nostrud itation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in repr',
            thumbnail: Thumbnail,
            link: 'https://www.gameloft.vn',
        },
        {
            title: 'Short title here 5',
            desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua'
              + 'Ut enim ad minim veniam, quis nostrud itation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in repr',
            thumbnail: Thumbnail,
            link: 'https://www.gameloft.vn',
        },
        {
            title: 'Short title here 6',
            desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua'
              + 'Ut enim ad minim veniam, quis nostrud itation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in repr',
            thumbnail: Thumbnail,
            link: 'https://www.gameloft.vn',
        },
        {
            title: 'Short title here 7',
            desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua'
              + 'Ut enim ad minim veniam, quis nostrud itation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in repr',
            thumbnail: Thumbnail,
            link: 'https://www.gameloft.vn',
        },
        {
            title: 'Short title here 8',
            desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua'
              + 'Ut enim ad minim veniam, quis nostrud itation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in repr',
            thumbnail: Thumbnail,
            link: 'https://www.gameloft.vn',
        },
    ];
    const SliderItem = ({itemData, itemIndex}) => {
        if (!isMobile) {
            if (itemIndex == hoverItem) {
                return (
                    <div className="event-carousel__item" key={itemIndex} onMouseLeave={()=> sethoverItem(null)}>
                        <div className="event-carousel__item-title">{itemData.title}</div>
                        <div className="event-carousel__item-border"></div>
                        <TextTruncate
                            className="event-carousel__item-desc"
                            line={3}
                            element="span"
                            truncateText="…"
                            text={itemData.desc}
                        />
                        <div className="event-carousel__item-button">
                            Read more
                        </div>
                        <div className="event-carousel__item-link d-flex justify-content-center align-items-center" onMouseEnter={()=> sethoverItemIcon(itemIndex)} onMouseLeave={()=> sethoverItemIcon(null)}>
                            {
                                hoverItemIcon === itemIndex
                                    ? <LinkActive/>
                                    :  <Link/>
                            }
                        </div>
                    </div>
                )
            } else {
                return (
                    <div className="event-carousel__item" key={itemIndex} onMouseEnter={()=> sethoverItem(itemIndex)}>
                        <div className="event-carousel__item-thumbnail-wrapper">
                            <img src={itemData.thumbnail} className="event-carousel__item-thumbnail"/>
                        </div>
                        <div className="event-carousel__item-title">{itemData.title}</div>
                    </div>
                )
            }
        } else {
            return (
                <div className="event-carousel__item" key={itemIndex}>
                    <div className="event-carousel__item-title">{itemData.title}</div>
                    <div className="event-carousel__item-border"></div>
                    <TextTruncate
                        className="event-carousel__item-desc"
                        line={3}
                        element="span"
                        truncateText="…"
                        text={itemData.desc}
                    />
                    <div className="event-carousel__item-button">
                        Read more
                    </div>
                </div>
            )

        }

    }
    return(
        <div className="event-carousel__wrapper">
            <Slider {...carouselSettings}>
                {
                    eventItem.map((item, index)=>
                        <SliderItem itemData={item} itemIndex={index} key={index}/>
                    )
                }
            </Slider>
        </div>
    );
}

export default EventSlider;
